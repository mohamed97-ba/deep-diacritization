## Objective 
Automatic diacritization of Arabic text is one of the most challenging tasks in Arabic natural language processing
this model aims to annotate arabic text with diacritics . Given the correct diacritics for each word, there will not be
much ambiguity in Arabic text, which can help many applications such as Text-to-Speech (TTS)

## Quick Introduction
The dataset has 15 sets of labels : 3 harakāt in 4 variants (with tanwı̄n, shadda, tanwı̄n and shadda, and neither), the
sukūn, and the plain shadda.
"No Diacritic', 'Fatha', 'Fathatan', 'Damma', 'Dammatan', 'Kasra', 'Kasratan', 'Sukun', 'Shadda', 'Shadda + Fatha', 'Shadda + Fathatan', 'Shadda + Damma', 'Shadda + Dammatan', 'Shadda + Kasra', 'Shadda + Kasratan"

## The Approach
The core is a two-level recurrence hierarchy (D2) that operates on the word and character levels separately enabling faster training and inference than comparable traditional models. A cross-level attention module further connects the two, and opens the door for network interpretability. The task module is a softmax classifier that enumerates valid combinations of diacritics. This architecture can be extended with a recurrent decoder (D3) that optionally accepts priors from partially diacritized text, improving performance significantly. We employ extra tricks such as sentence dropout and majority voting to further boost the final result. 

## Instructions to run the code
Code could be run using google colab .

## Environment Setup
You'll find a requirement file that you could install in your own virtual environment.
```shell
pip install -r requirements.txt
```

#### 1. Download Tashkeela Dataset 
Download data from AliOSm repository, the dataset contains:
- train.txt - Contains 50,000 lines of diacritized Arabic text which can be used as training dataset
- val.txt - Contains 2,500 lines of diacritized Arabic text which can be used as validation dataset
- test.txt - Contains 2,500 lines of diacritized Arabic text which can be used as testing dataset
```shell
bash scripts/download_tashkeela.sh
```

#### 2. Download fastText Arabic CC Binary
we download the fastText features pretrained on CommonCrawl Arabic data, it will create the vocab files with words and embedding vectors
```shell
bash scripts/download_fasttext_ar.sh
``` 

#### 3. Segment Datasets
we use a sliding context window of size Ts on each sentence. A given sentence is split into several overlapping segments each of which is
given separately to the model during training. This works well as the local context is often sufficient for correct inference. During inference, the same sequence of characters may appear in different contexts (different segments from one sentence) and potentially lead to different diacritized forms. To choose the
final diacritic, we use a popularity voting mechanism and,  The values chosen for Ts , Tw and the stride are: Ts = 10 with stride = 1 for training and
validation and Ts = 20 with stride = 2 for evaluation/testing; and Tw = 13 for both training and evaluation.
```shell
bash scripts/segment_train_val.sh
``` 
```shell
bash scripts/segment_test.sh
``` 

#### 4. Extract and Embed Tashkeela Vocabulary
```shell
bash scripts/embed_vocab.sh
```

#### 5. Train D2 Model
You need to train first the D2 model (for around 20 epochs) 
The model is left to converge until the validation loss does not improve for 3 consecutive epochs where each epoch enumerates a randomly shuffled version of the training segments exactly once. The learning rate is reduced by half when the validation loss does not improve for one epoch. We train with
a mini-batch size of 128 segments.
```shell
bash scripts/train.sh d2
```
#### 6. Train D3 Model
This model is not trained from scratch, but uses the weights of the encoders and character embeddings learned from D2. Those weights are kept frozen and only the decoder and classifier are trained.we train for a further 10 epochs
```shell
bash scripts/train.sh d3
```

#### 6. Predict then Evaluate Model
We use the script provided by (Fadel et al., 2019a) to evaluate our results. To be consistent with prior work, we report our results in terms of both word error-rate (WER) and diacritic error-rate (DER), with and without case-endings, as well as including and excluding characters with no diacritics.
```shell
bash scripts/evaluate.sh d2
```
```shell
bash scripts/evaluate.sh d3
```
#### 7. Hyperparameters
- Name : DiacritizerD2/D3
- learning rate = 0.002
- batch size = 128 / 75
- Adam optimizer
- max-word-len: 13
- max-sent-len: 10
## Pretrained Models
- [D2 Pretrained Model](https://drive.google.com/file/d/1FGelqImFkESbTyRsx_elkKIOZ9VbhRuo/view?usp=sharing)


