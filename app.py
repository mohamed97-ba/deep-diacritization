import os
import yaml
import torch as T 

from model_d2 import DiacritizerD2 as D2
from model_d3 import DiacritizerD3 as D3
from data_utils import DatasetUtils
from predict import predict as pred



from fastapi import FastAPI

app = FastAPI()
# constants
MODEL_PATH = "/content/drive/MyDrive/deep-diacritization-main/models/tashkeela-d2/tashkeela-d2.best.pt"
CONFIG_PATH = "/content/drive/MyDrive/deep-diacritization-main/configs/config_d2.yaml"


config = None 
model = None 
data_utils = None 


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get('/predict')
def predict(text : str):
    global model, data_utils, config
    #text = request.form["text"]
    #text = "قافلة الماضي لن تصل بك إلى أي مكان."
    #if request.method == 'POST' and text.strip() != "":
    if model is None:
        with open(CONFIG_PATH, 'r') as fin: 
            config = yaml.load(fin, Loader=yaml.FullLoader)
        data_utils = DatasetUtils(config)
        model = D2(config)
        model.build(data_utils.embeddings, len(data_utils.letter_list))
        model.load_state_dict(T.load(MODEL_PATH, map_location=T.device('cpu'))["state_dict"])
        model.eval() 
    output = pred(text, model, data_utils, config)
    return {"sentence_diac" : output, "sentence_undiac" : text}

#if __name__ == '__main__':
 #  app.run()
