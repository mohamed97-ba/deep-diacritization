import torch as T
from pyarabic.araby import tokenize, strip_tashkeel, separate
import numpy as np
from collections import Counter
from model_d2 import DiacritizerD2 as D2
from model_d3 import DiacritizerD3 as D3
from tqdm import tqdm
import os
import yaml
from data_utils import DatasetUtils

# constants
STRIDE = 1
WINDOW_SZ = 20
MIN_WINDOW_SZ = 1

MODEL_PATH = "./models/tashkeela-d2/tashkeela-d2.best.pt"
CONFIG_PATH = "./configs/config_d2.yaml"

def flat_2_3head(output):
    haraka, tanween, shadda = [], [], []

    # 0, 1,  2, 3,  4, 5,  6, 7, 8,  9,     10,  11,   12,  13,   14
    # 0, F, FF, K, KK, D, DD, S, Sh, ShF, ShFF, ShK, ShKK, ShD, ShDD

    convert = [
        [0,0,0],
        [1,0,0],
        [1,1,0],
        [2,0,0],
        [2,1,0],
        [3,0,0],
        [3,1,0],
        [4,0,0],
        [0,0,1],
        [1,0,1],
        [1,1,1],
        [2,0,1],
        [2,1,1],
        [3,0,1],
        [3,1,1]
    ]

    b, ts, tw = output.shape

    for b_idx in range(b):
        h_s, t_s, s_s = [], [], []
        for w_idx in range(ts):
            h_w, t_w, s_w = [], [], []
            for c_idx in range(tw):
                c = convert[int(output[b_idx, w_idx, c_idx])]
                h_w  += [c[0]]
                t_w += [c[1]]
                s_w  += [c[2]]
            h_s += [h_w]
            t_s += [t_w]
            s_s += [s_w]
        
        haraka  += [h_s]
        tanween += [t_s]
        shadda  += [s_s]
            

    return haraka, tanween, shadda

def segment_text(raw_text):
    segments, mapping = [], []
    real_seg_idx = 0
    lines = raw_text.split("\n")
    for sent_idx, line in enumerate(lines):
        tokens = tokenize(line)
        if len(tokens) == 0:
            continue
        seg_idx, idx = 0, 0
        while idx < len(tokens):
            window = tokens[idx:idx+WINDOW_SZ]
            if WINDOW_SZ == -1:
                window = tokens
            if len(window) < MIN_WINDOW_SZ and seg_idx != 0:
                break

            segment = ' '.join(window)
            segments += [segment]
            char_offset = len(strip_tashkeel(' '.join(tokens[:idx])))

            if seg_idx > 0:
                char_offset += 1

            seg_tokens = tokenize(strip_tashkeel(segment))

            j = 0
            for st_idx, st in enumerate(seg_tokens):
                for _ in range(len(st)):
                    mapping += [(sent_idx, real_seg_idx,
                                 st_idx, j+char_offset)]
                    j += 1
                j += 1

            real_seg_idx += 1
            seg_idx += 1

            if STRIDE == -1:
                break
            idx += WINDOW_SZ if STRIDE >= WINDOW_SZ else STRIDE
    
    
    
    return segments, mapping


def encode(line, data_utils, pad=True):
    tokens = tokenize(line.strip())

    word_x = []
    char_x = []
    char_y = []

    for word in tokens:
        split_word = data_utils.split_word_on_characters_with_diacritics(word)
        
        cx, _, cy = data_utils.create_label_for_word(split_word)

        word_strip = strip_tashkeel(word)
        word_x += [data_utils.w2idx[word_strip]
                   if word_strip in data_utils.w2idx else data_utils.w2idx["<pad>"]]

        if pad:
            char_x += [data_utils.pad_sequence(cx, data_utils.max_word_len)]
            char_y += [data_utils.pad_sequence(cy, data_utils.max_word_len, pad=[data_utils.pad_target_val]*3)]
        else:
            char_x += [cx]
            char_y += cy + [[0,0,0]]
    
    if pad:
        diac_x = data_utils.create_decoder_input(char_y)
    else:
        diac_x = char_y

    max_slen = data_utils.max_sent_len
    max_wlen = data_utils.max_word_len
    p_val = data_utils.pad_val

    if pad:
        word_x = data_utils.pad_sequence(word_x, max_slen)
        char_x = data_utils.pad_sequence(char_x, max_slen, pad=[p_val]*max_wlen)
        diac_x = data_utils.pad_sequence(diac_x, max_slen, pad=[[p_val]*8]*max_wlen)
    return word_x, char_x, diac_x


def shakkel_char(diac: int, tanween: bool, shadda: bool) -> str:
    diacritics = {
        "FATHA": 1,
        "KASRA": 2,
        "DAMMA": 3,
        "SUKUN": 4
    }
    returned_text = ""
    if shadda and diac != diacritics["SUKUN"]:
        returned_text += "\u0651"

    if diac == diacritics["FATHA"]:
        returned_text += "\u064E" if not tanween else "\u064B"
    elif diac == diacritics["KASRA"]:
        returned_text += "\u0650" if not tanween else "\u064D"
    elif diac == diacritics["DAMMA"]:
        returned_text += "\u064F" if not tanween else "\u064C"
    elif diac == diacritics["SUKUN"]:
        returned_text += "\u0652"

    return returned_text


def predict(raw_text, model, data_utils, config):
    # segment and create mapping
    segment_results = []
    n = 10
    

    raw_text_segments = [tokenize(raw_text)[i:i+n] for i in range(0, len(tokenize(raw_text)), n)]
    
    for raw_text_segment in tqdm(raw_text_segments):
        temp_raw_text = " ".join(raw_text_segment)
        segments, mapping_list = segment_text(temp_raw_text)
        
        mapping = data_utils.load_mapping_v3(mapping_list)
        original_lines = data_utils.load_file_clean(temp_raw_text.split('\n'), strip=True)
        
        original_lines_w_diacs = data_utils.load_file_clean(temp_raw_text.split('\n'))
        
        # encode segments
        batch_size = 3
        haraka, tanween, shadda = [], [], []
        for segment_index in range(0, len(segments), batch_size):
            sents, words, hints = [], [], []
            for segment in segments[segment_index:segment_index+batch_size+1]:
                word_x, char_x, char_y = encode(segment, data_utils)
                
                sents += [word_x]
                words += [char_x]
                hints += [char_y]
               
            # run inference
            
            diac = model(T.tensor(sents), T.tensor(words), T.tensor(hints))[0]
            
            output = np.argmax(T.softmax(diac.detach(), dim=-1).cpu().numpy(), axis=-1)
            temp_haraka, temp_tanween, temp_shadda = flat_2_3head(output)
            haraka += temp_haraka
            tanween += temp_tanween
            shadda += temp_shadda

        # haraka = T.softmax(diac[0], dim=-1).detach().numpy()
        # tanween = T.sigmoid(diac[1]).detach().numpy()
        # shadda = T.sigmoid(diac[2]).detach().numpy()
        # majority voting
        diacritized_lines = []
        for sent_idx, line in enumerate(original_lines):
            diacritized_line = ""
            line = ' '.join(tokenize(line))
            _, _, out_y = encode(original_lines_w_diacs[sent_idx], data_utils, pad=False)
            for char_idx, char in enumerate(line):
                diacritized_line += char
                if sum(out_y[char_idx]) > 0:
                    diacritized_line += shakkel_char(*out_y[char_idx])
                    continue
            
                char_vote_haraka, char_vote_shadda, char_vote_tanween = [], [], []
                if sent_idx not in mapping:
                    continue
                for seg_idx in mapping[sent_idx]:
                    for t_idx in mapping[sent_idx][seg_idx]:
                        if char_idx in mapping[sent_idx][seg_idx][t_idx]:
                            c_idx = mapping[sent_idx][seg_idx][t_idx].index(
                                char_idx)
                            char_vote_haraka += [haraka[seg_idx][t_idx][c_idx]]
                            char_vote_tanween += [tanween[seg_idx][t_idx][c_idx]]
                            char_vote_shadda += [shadda[seg_idx][t_idx][c_idx]]

                if len(char_vote_haraka) > 0:
                    char_mv_diac = Counter(char_vote_haraka).most_common()[0][0]
                    char_mv_shadda = Counter(char_vote_shadda).most_common()[0][0]
                    char_mv_tanween = Counter(
                        char_vote_tanween).most_common()[0][0]
                    diacritized_line += shakkel_char(
                        char_mv_diac, char_mv_tanween, char_mv_shadda)

            diacritized_lines += [diacritized_line.strip()]

        segment_results.append('\n'.join(diacritized_lines))
        
    
    return " ".join(segment_results)




if __name__ == '__main__':
    text = "كن قويا بما يكفي لمواجهة العالم كل يوم."
    with open(CONFIG_PATH, 'r') as fin: 
        config = yaml.load(fin, Loader=yaml.FullLoader)
    data_utils = DatasetUtils(config)
    model = D2(config)
    model.build(data_utils.embeddings, len(data_utils.letter_list))
    model.load_state_dict(T.load(MODEL_PATH, map_location=T.device('cpu'))["state_dict"])
    model.eval() 
   
        
output = predict(text, model, data_utils, config)
    #with open(os.path.join(config["paths"]["base"], 'preds', 'predictions_tunizi.txt'), 'w', encoding='utf-8') as fout:
     #   fout.write(output)
print(output)

